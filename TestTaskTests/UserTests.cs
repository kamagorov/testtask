using Moq;
using NUnit.Framework;
using System;
using System.Threading.Tasks;
using TestTask;
using TestTask.Authentication;

namespace TestTaskTests
{
    public class Tests
    {
        private const string _username = "testuser";
        private const string _password = "123456";
        private const int _minToExpire = 1;

        private Mock<IAuthenticationService> _authencationService;
        private User _user;

        [SetUp]
        public void Setup()
        {
            _authencationService = new Mock<IAuthenticationService>();
            _authencationService.SetupSequence(p => p.Authenticate(_username, _password)).Returns(
                    new AuthToken()
                    {
                        Token = Guid.NewGuid().ToString(),
                        ExpiresAt = DateTime.UtcNow.AddMinutes(_minToExpire)
                    }).Returns(
                    new AuthToken()
                    {
                        Token = Guid.NewGuid().ToString(),
                        ExpiresAt = DateTime.UtcNow.AddMinutes(_minToExpire)
                    }
                );

            _user = new User(_username, _password, _authencationService.Object);
        }

        [Test]
        public void GetTokenTask()
        {
            Assert.NotNull(_user.AuthToken);
        }

        [Test]
        public async Task GetTokenIfTimeExpiredTest()
        {
            string token = _user.AuthToken;
            await Task.Delay(_minToExpire * 60 * 1000 + 1000);

            Assert.AreNotEqual(token, _user.AuthToken);
        }
    }
}