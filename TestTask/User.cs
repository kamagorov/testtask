﻿using System;
using TestTask.Authentication;

namespace TestTask
{
    public class User : IUser
    {
        public readonly string Username;
        public readonly string Password;

        private AuthToken _authToken;
        private readonly IAuthenticationService _authenticationService;

        public User(string username, string password, IAuthenticationService authenticationService)
        {
            Username = username;
            Password = password;
            _authenticationService = authenticationService;
        }

        public string AuthToken
        {
            get
            {
                if (_authToken is null || _authToken.ExpiresAt < DateTime.UtcNow)
                {
                    _authToken = _authenticationService.Authenticate(Username, Password);
                }

                return _authToken.Token;
            }
        }
    }
}
