﻿using System;

namespace TestTask
{
    interface IUser
    {
        /// <summary>
        /// User's authentication token.
        /// </summary>
        String AuthToken { get; }
    }

}
